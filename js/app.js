console.log('load app.js')

window.onload = () => {
  //  ***  init ***
  const main = document.querySelector('.main')
  const userEmail = document.querySelector('.email')
  const userPassword = document.querySelector('.password')
  const form = document.querySelector('.form')
  const title = document.querySelector('.form__title')
  const btnSend = document.querySelector('.form__btn-main')
  const email = document.querySelector('.form__email')
  const pwd = document.querySelector('.form__password')
  const checkbox = document.querySelector('.form__checkbox')
  const btnChange = document.querySelector('.form__btn-change-form')
  const txt = {
    auth: 'Авторизоваться',
    register: 'Зарегистрироваться',
    enter: 'Вход',
    reg: 'Регистрация'
  }

  // *** events ***
  btnChange.addEventListener('click', (e) => {
    e.preventDefault();

    // reset all
    [email, pwd, checkbox].forEach(resetErr);
    [email, pwd, checkbox].forEach(resetVal)

    // change form
    if (btnChange.innerText === txt.auth) {
      btnChange.innerText = txt.register
      title.innerText = txt.enter
      btnSend.innerText = txt.enter
      return
    }

    if (btnChange.innerText === txt.register) {
      btnChange.innerText = txt.auth
      title.innerText = txt.reg
      btnSend.innerText = txt.reg
      return
    }
  })

  btnSend.addEventListener('click', () => {
    // reset err
    [email, pwd, checkbox].forEach(resetErr)
    if (isFormInvalid()) {
      return
    }
    const user = {
      email: email.value,
      password: pwd.value
    }

    if (btnChange.innerText === txt.auth) {
      register(user);
      return
    }
    if (btnChange.innerText === txt.register) {
      auth(user)
      return
    }
  })

  // *** logic ***
  // isEmail
  const EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu

  function isEmail(value) {
    return EMAIL_REGEXP.test(value)
  }

  // reset error
  function resetErr(e) {
    e.classList.remove('border-err')

    const field = e.classList[0].split('__')[1]

    const err = document.querySelector(`.form__${field}-err`)
    err.classList.add('disable')

    const star = document.querySelector(`.required-${field}`)
    star.classList.remove('err')

    const label = document.querySelector(`.title-${field}`)
    label && label.classList.remove('err')
  }
  // reset values
  function resetVal(e) {
    if (e.type === 'text' || e.type === 'password') {
      e.value = ''
      // e.value = '12345@gmail.com'
    }
    if (e.type === 'checkbox') {
      e.checked = false
      // e.checked = true
    }
  }

  // showErr
  function showErr(e, msg) {
    const err = document.querySelector('.' + e.className + '-err')
    err.innerText = msg || 'Поле обязательно для заполнения'

    const field = e.className.split('__')[1]
    const title = document.querySelector(`.title-${field}`)

    if (title) {
      title.classList.add('err')
    }

    const input = document.querySelector(`.form__${field}`)
    input.classList.add('border-err')

    const star = document.querySelector(`.required-${field}`)
    star.classList.add('err')

    err.classList.remove('disable')
  }

  // isFormValid
  function isFormInvalid() {
    return [email, pwd, checkbox].find(e => {
      if ((((e.type === 'text') || (e.type === 'password')) && !e.value.trim()) ||
        ((e.type === 'checkbox') && !e.checked)) {
        showErr(e)
        return e
      }

      if ((e === email) && !isEmail(email.value)) {
        showErr(e, 'Email невалидный')
        return e
      }

      if ((e === pwd) && (e.value.length < 8)) {
        showErr(e, 'Пароль должен содержать как минимум 8 символов')
        return e
      }
    })
  }

  // register
  function register(user) {
    try {
      let users = JSON.parse(localStorage.getItem("users"))

      if (Array.isArray(users)) {
        users = users.filter(e => e.email !== user.email)
        users.push(user)
        localStorage.setItem("users", JSON.stringify(users))
        alert('Пользователь успешно зарегистрирован [' + user.email + ':' + user.password + ']')
      } else {
        localStorage.setItem("users", JSON.stringify([user]))
        alert('Пользователь успешно зарегистрирован [' + user.email + ':' + user.password + ']')
      }
    } catch (err) {
      console.log(err)
      localStorage.setItem("users", JSON.stringify([user]))
    }
  }

  // auth
  function auth(user) {
    try {
      const users = JSON.parse(localStorage.getItem("users")) || []

      const isExist = users.some(e => e.email === user.email && e.password === user.password)

      if (isExist) {
        success(user)
      } else {
        console.log('Показываем ошибку', user, users);

        [email, pwd].forEach(e => {
          showErr(e, e === pwd ? 'Логин или Пароль невереный' : ' ')
        })
      }
    } catch (err) {
      console.log(err)

      [email, pwd].forEach(e => {
        showErr(e, e === pwd ? 'Логин или Пароль невереный' : ' ')
      })
    }
  }

  // авторизация
  function success(user) {
    form.classList.add('hidden')
    const body = document.getElementsByTagName('body')[0]
    body.classList.add('success')

    main.classList.remove('hidden')
    userEmail.innerText = user.email
    userPassword.innerText = user.password
  }
}